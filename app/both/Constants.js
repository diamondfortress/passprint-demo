"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Constants = /** @class */ (function () {
    function Constants() {
    }
    Constants.EMPTY_STRING = "";
    Constants.SESSION = {
        LANGUAGE: "language",
        LOADING: "isLoading",
        PLATFORM_READY: "platformReady",
        TRANSLATIONS_READY: "translationsReady",
        INCORRECT_PASSWORD: "incorrectPassword",
        FORGOT_PASSWORD: "forgotPassword",
        CREATE_ACCOUNT: "createAccount",
        RESET_PASSWORD: "resetPassword",
        REGISTERED_ERROR: "registeredError",
        NOT_REGISTERED_ERROR: "notRegisteredError",
        RESET_PASSWORD_ERROR: "resetPasswordError",
        RESET_PASSWORD_ERROR_MESSAGE: "resetPasswordErrorMessage",
        RESET_PASSWORD_TOKEN: "resetPasswordToken",
        WAS_PASSWORD_RESET: "wasPasswordReset",
        EMAIL: "email"
    };
    Constants.DEVICE = {
        IOS: "iOS",
        ANDROID: "Android"
    };
    Constants.STYLE = {
        IOS: "ios",
        MD: "md"
    };
    Constants.METEOR_ERRORS = {
        NO_PASSWORD: "User has no password set",
        USER_NOT_FOUND: "User not found",
        INCORRECT_PASSWORD: "Incorrect password",
        EMAIL_EXISTS: "Email already exists.",
        TOKEN_EXPIRED: "Token expired"
    };
    Constants.ADD_IMAGE_PLACEHOLDER_URI = "/images/add_image_camera_photo.png";
    Constants.IMAGE_URI_PREFIX = "data:image/jpeg;base64,";
    return Constants;
}());
exports.Constants = Constants;
