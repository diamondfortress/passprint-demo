import {Constants} from "../../both/Constants";

Accounts.validateNewUser(function (user:Meteor.User) {
    var email:string;
    if (user && user.services) {
        if (user.services.google && user.services.google.email) {
            email = user.services.google.email;
        } else if (user.services.facebook && user.services.facebook.email) {
            email = user.services.facebook.email;
        } else if (user.services.passprint && user.services.passprint.email) {
            email = user.services.passprint.email.address;
        }
    }
    if (email) {
        var meteorUser:Meteor.User = Accounts.findUserByEmail(email);
        if (meteorUser) {
            var provider;
            if (meteorUser.services.password) {
                provider = Meteor.settings.public["appName"];
            } else if (meteorUser.services.google) {
                provider = "Google";
            } else if (meteorUser.services.facebook) {
                provider = "Facebook";
            } else if (meteorUser.services.passprint) {
                provider = "PassPrint";
            }
            throw new Meteor.Error("email-registered", "This account signs in with " + provider + ".");
        }
    }
    return true;
});

Accounts.onCreateUser(function (options, user) {
    if (options.profile) {
        user.profile = options.profile;
    }
    // Copy oauth information to meteor profile
    if (user && user.services) {
        if (user.services.google) {
            var google = user.services.google;
            user.emails = [{
                address: google.email,
                verified: google.verified_email
            }];
            user.profile.name = {
                display: google.name,
                given: google.given_name,
                family: google.family_name
            };
            user.profile.picture = google.picture;
        } else if (user.services.facebook) {
            var facebook = user.services.facebook;
            user.emails = [{
                address: facebook.email,
                verified: false
            }];
            user.profile.name = {
                display: facebook.name,
                given: facebook.first_name,
                family: facebook.last_name
            };
        } else if (user.services.passprint) {
            var passprint = user.services.passprint;
            user.emails = [passprint.email];
            user.profile.name = passprint.name;
            user.profile.picture = passprint.profile_picture
        }
    }
    return user;
});

Accounts.emailTemplates.siteName = Meteor.settings.public["appName"];
Accounts.emailTemplates.from = "Ionic2-Meteor-Login Accounts <contact@localhost.com>";
Accounts.emailTemplates.resetPassword.subject = function (user) {
    return "Password Reset Requested";
};
Accounts.emailTemplates.resetPassword.html = function (user, url) {
    console.log("reset password url: " + url);
    var token = url.split("reset-password/")[1];
    console.log("token: " + token);
    var href:string = Meteor.absoluteUrl()
        + Meteor.settings.public["routes"]["resetPassword"] + token;
    var userLang = "en";
    var emailHtml = Constants.EMPTY_STRING;
    var buttonStyle:string = "display: inline-block; padding: 6px 12px; margin-bottom: 0; font-size: 14px; " +
        "font-weight:400; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; line-height: 1.42857143; " +
        "text-align: center; white-space: nowrap; vertical-align: middle; -ms-touch-action: manipulation; " +
        "touch-action: manipulation; cursor: pointer; -webkit-user-select: none; -moz-user-select: none; " +
        "-ms-user-select: none; user-select: none; background-image: none; border: 1px solid transparent; " +
        "border-radius: 4px; color: #fff; background-color: #337ab7; border-color: #2e6da4; text-decoration: none;";
    if (userLang === "en") {
        emailHtml = "Hello " + user.profile.name.display + ", <br><br>Open this email on your mobile device " +
            "and tap the button below to open the application and reset your password." +
            "<br><br><a href='" + href + "' style='" + buttonStyle + "' href='" + href + "'>Reset Password</a>" +
            "<br><br>Thanks for using " + Meteor.settings.public["appName"] + "!";
    } else if (userLang === "es") {
        emailHtml = "Hola " + user.profile.name.display + ", <br><br>Abre este correo electrónico en tu dispositivo móvil " +
            "y pulse el botón de abajo para abrir la aplicación y restablecer su contraseña." +
            "<br><br><a href='" + href + "' style='" + buttonStyle + "'>Restablecer la Contraseña</a>" +
            "<br><br>Gracias por usar " + Meteor.settings.public["appName"] + "!";
    }
    return emailHtml;
};
