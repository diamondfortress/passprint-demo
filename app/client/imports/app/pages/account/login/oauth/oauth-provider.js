"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var home_1 = require("../../../home/home");
var ToastMessenger_1 = require("../../../../utils/ToastMessenger");
var Constants_1 = require("../../../../../../../both/Constants");
var oauth_provider_html_1 = require("./oauth-provider.html");
var OauthProviderComponent = /** @class */ (function () {
    function OauthProviderComponent(nav, translate) {
        this.nav = nav;
        this.translate = translate;
    }
    OauthProviderComponent.prototype.loginWithProvider = function (provider) {
        console.log("loginWith" + provider);
        var component = this;
        switch (provider) {
            case "Google":
                Meteor.loginWithGoogle({
                    requestPermissions: [
                        'https://www.googleapis.com/auth/userinfo.profile',
                        'https://www.googleapis.com/auth/userinfo.email'
                    ],
                }, function (error) {
                    if (error) {
                        console.log("Error: " + JSON.stringify(error));
                        var errorMessage = error.message;
                        if (error.error === "email-registered") {
                            errorMessage = error.reason;
                        }
                        else if (error.reason === Constants_1.Constants.METEOR_ERRORS.EMAIL_EXISTS) {
                            errorMessage = component.translate.instant("create-account-card.errors.alreadyRegistered");
                        }
                        else if (error.errorType === "Accounts.LoginCancelledError") {
                            errorMessage = null;
                        }
                        if (errorMessage) {
                            console.log("Error signing in with Google: " + errorMessage);
                            new ToastMessenger_1.ToastMessenger().toast({
                                type: "error",
                                message: errorMessage,
                                title: component.translate.instant("login-card.errors.signIn")
                            });
                        }
                    }
                    else {
                        console.log("Successfully signed in with Google");
                        component.nav.setRoot(home_1.HomePage);
                    }
                });
                break;
            case "Facebook":
                Meteor.loginWithFacebook({
                    requestPermissions: ['email', 'user_birthday', 'user_location'],
                }, function (error) {
                    if (error) {
                        console.log("Error: " + JSON.stringify(error));
                        var errorMessage = error.message;
                        if (error.error === "email-registered") {
                            errorMessage = error.reason;
                        }
                        else if (error.reason === Constants_1.Constants.METEOR_ERRORS.EMAIL_EXISTS) {
                            errorMessage = component.translate.instant("create-account-card.errors.alreadyRegistered");
                        }
                        else if (error.errorType === "Accounts.LoginCancelledError") {
                            errorMessage = null;
                        }
                        if (errorMessage) {
                            console.log("Error signing in with Facebook: " + errorMessage);
                            new ToastMessenger_1.ToastMessenger().toast({
                                type: "error",
                                message: errorMessage,
                                title: component.translate.instant("login-card.errors.signIn")
                            });
                        }
                    }
                    else {
                        console.log("Successfully signed in with Facebook");
                        component.nav.setRoot(home_1.HomePage);
                    }
                });
                break;
            case "PassPrint":
                Meteor.loginWithPassPrint({
                    requestPermissions: ["email", "name", "profile_picture"],
                }, function (error) {
                    if (error) {
                        console.log("Error: " + JSON.stringify(error));
                        var errorMessage = error.message;
                        if (error.error === "email-registered") {
                            errorMessage = error.reason;
                        }
                        else if (error.reason === Constants_1.Constants.METEOR_ERRORS.EMAIL_EXISTS) {
                            errorMessage = component.translate.instant("create-account-card.errors.alreadyRegistered");
                        }
                        else if (error.errorType === "Accounts.LoginCancelledError") {
                            errorMessage = null;
                        }
                        if (errorMessage) {
                            console.log("Error signing in with PassPrint: " + errorMessage);
                            new ToastMessenger_1.ToastMessenger().toast({
                                type: "error",
                                message: errorMessage,
                                title: component.translate.instant("login-card.errors.signIn")
                            });
                        }
                    }
                    else {
                        console.log("Successfully signed in with PassPrint");
                        component.nav.setRoot(home_1.HomePage);
                    }
                });
                break;
            default:
                console.log("Provider not listed");
        }
    };
    __decorate([
        core_1.Input()
    ], OauthProviderComponent.prototype, "oauthProvider", void 0);
    OauthProviderComponent = __decorate([
        core_1.Component({
            selector: 'oauth-provider',
            template: oauth_provider_html_1.default
        })
    ], OauthProviderComponent);
    return OauthProviderComponent;
}());
exports.OauthProviderComponent = OauthProviderComponent;
