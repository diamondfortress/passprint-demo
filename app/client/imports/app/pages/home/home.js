"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var angular2_meteor_1 = require("angular2-meteor");
var Constants_1 = require("../../../../../both/Constants");
var home_html_1 = require("./home.html");
var HomePage = /** @class */ (function (_super) {
    __extends(HomePage, _super);
    function HomePage(app, nav, translate) {
        var _this = _super.call(this) || this;
        _this.app = app;
        _this.nav = nav;
        _this.translate = translate;
        return _this;
    }
    HomePage.prototype.ngOnInit = function () {
        var _this = this;
        // Use MeteorComponent autorun to respond to reactive session variables.
        this.autorun(function () {
            _this.user = Meteor.user();
            // Wait for translations to be ready
            // in case component loads before the language is set
            // or the language is changed after the component has been rendered.
            // Since this is the home page, this component and any child components
            // will need to wait for translations to be ready.
            if (Session.get(Constants_1.Constants.SESSION.TRANSLATIONS_READY)) {
                _this.translate.get('page-home.title').subscribe(function (translation) {
                    // Set title of web page in browser
                    _this.app.setTitle(translation);
                });
            }
        });
    };
    HomePage = __decorate([
        core_1.Component({
            selector: 'page-home',
            template: home_html_1.default
        })
    ], HomePage);
    return HomePage;
}(angular2_meteor_1.MeteorComponent));
exports.HomePage = HomePage;
