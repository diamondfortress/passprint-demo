
import {Constants} from "../../both/Constants";
declare var accountsDoneCallback;

Session.set(Constants.SESSION.RESET_PASSWORD_TOKEN, false);

Accounts.onResetPasswordLink(function (token, done) {
    console.log("onResetPasswordLink()");
    Session.set(Constants.SESSION.RESET_PASSWORD_TOKEN, token);
    accountsDoneCallback = done;
});

Accounts.onLoginFailure(function () {
    console.log("Failed to log in.");
});
