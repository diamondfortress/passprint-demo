#!/usr/bin/env bash
# This .sh file will be sourced before starting your application.
# You can use it to put environment variables you want accessible
# to the server side of your app by using process.env.MY_VAR
#
# Example:
# export MONGO_URL="mongodb://localhost:27017/myapp-development"
# export ROOT_URL="http://localhost:3000"

export METEOR_SETTINGS=$(cat settings.json)
export PORT="6000"
export ROOT_URL="https://demo.passprint.me"
export MOBILE_DDP_URL=$ROOT_URL
export MAIL_URL=""
export MONGO_URL="mongodb://passprint-demo-admin:P%40%24%24Pr1ntD3m0P%40%24%24w0rd@ds127994.mlab.com:27994/passprint-demo"
